CXX=g++

##########
# Paths
##########

SRC_PATH	:= src
OBJ_PATH	:= obj
DEP_PATH	:= autodepends
TARGET_PATH := bin

objects = $(patsubst $(SRC_PATH)%.cpp,$(OBJ_PATH)%.o,$(shell find $(SRC_PATH)/$(1) -name *.cpp))
makedirs = $(shell mkdir -p $(1))

SRC := $(shell find $(SRC_PATH) -name *.cpp)

TARGETS := simulator viewer



#########k
# Flags
##########
DBG_FLAGS	:= -g
OPT_FLAGS	:= -O3 -DNDEBUG
LIBS	:=	#-pthread
THIRD_PARTY :=
CXX_VERSION := -std=c++14
CXXFLAGS := $(CXX_VERSION) -Wall -Wextra -Wpedantic -I src $(THIRD_PARTY) $(LIBS)

#export MAKEFLAGS="-j 12"


.PHONY:	clean all info optimized debug

all: $(TARGETS)

optimized:
	$(eval CXXFLAGS += $(OPT_FLAGS))

debug:
	$(eval CXXFLAGS += $(DBG_FLAGS))

.SECONDEXPANSION:
$(TARGETS): $$(call objects,$$@)
	@mkdir -p $(TARGET_PATH)
	$(CXX) -o $(TARGET_PATH)/$@ $^

clean:
	rm -rf $(DEP_PATH) $(OBJ_PATH) $(TARGET_PATH)

ifneq ($(MAKECMDGOALS), clean)
-include $(SRC:$(SRC_PATH)%.cpp=$(DEP_PATH)%.d)
endif

$(DEP_PATH)/%.d:	$(SRC_PATH)/%.cpp
	@mkdir -p $(dir $@)
	$(CXX) -MM -MT $(OBJ_PATH)/$*.o -MF $@ $(CXXFLAGS) $<

$(OBJ_PATH)/%.o:	$(SRC_PATH)/%.cpp
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) -c $< -o $@
