#ifndef SIMULATOR_H_
#define SIMULATOR_H_
#include "util/matrix.h"
#include "util/metadata.h"

class Simulator {

    private:
        static constexpr metadata::data_t DEFAULT_EDGE = 0;
        static constexpr metadata::data_t DEFAULT_CENTER = 50;

        metadata::iteration_t _iterations = 0;
        metadata::epsilon_t _epsilon = .1;
        Matrix<metadata::data_t, 2> _matrix;
        int _checkpoint = 0;

        Simulator(metadata&& mdata);

        std::string checkpoint_filename() const { return "chkpt." + std::to_string(_iterations) + ".out"; }

    public:

        Simulator(std::istream& input_stream, const int& checkpoint = 0); 

        //Simulator(const size_t& rows, const size_t& cols);

        int run();

        size_t size() const { return _matrix.size(); }

        size_t row_length() const { return _matrix.size(0); }

        size_t col_length() const { return _matrix.size(1); }

        void read_from(std::istream* input_stream);

        void write_to(std::ostream& output_stream) const;

};

#endif

/*
Iteration number (uint32_t)
Epsilon (float)
Number of rows (uint32_t)
Number of columns (uint32_t)
The grid of floats in 2D row-major format with [0][0] being the cell, then [0][1], etc. When row 0 is done move on to row 1 ([1][0], [1][1], etc).
*/
