#include "simulator.h"

#include <unistd.h>
#include <stdlib.h>

int main(int argc, char * argv[]) {

//    int numrow, numcol;
//    numrow = atoi(argv[1]);
//    numcol = atoi(argv[2]);
    /*
    char option;
    int checkpoints = 0;
    while ((option = getopt (argc, argv, "c:")) != -1) {
        switch (option) {
            case 'c':
                checkpoints = atoi(optarg);
                break;
        }
    }
    */
    
    int checkpoints = (argc > 1) ? atoi(argv[1]) : 0;

    Simulator sim(std::cin, checkpoints);
    sim.run();
    sim.write_to(std::cout);

    //printf("%d iterations to reach stability\n", );
    return 0;

}
