#include "simulator.h"
#include <math.h>
#include <iostream>
#include <ctime>
#include <fstream>

int Simulator::run() {
    bool stable = false;
    Matrix<float, 2> swap_matrix(row_length(), col_length());

    while (!stable){
        stable = true;
        for (size_t i = 1; i < col_length() - 1; i++){
            for (size_t j = 1 ; j < row_length() - 1; j++){

                float current = _matrix.at(i, j);
                float next = (_matrix.at(i - 1, j) + _matrix.at(i, j - 1) + _matrix.at(i + 1, j) + _matrix.at(i, j + 1)) / 4;

                swap_matrix.at(i, j) = next;

                if (fabs(next - current) >= _epsilon)
                    stable = false;
            }
        }
        _matrix.swap(swap_matrix);
        _iterations++;

        if (_checkpoint == 0? false: _iterations % _checkpoint == 0) {
            std::fstream checkpt(checkpoint_filename(), std::fstream::out);
            write_to(checkpt);
            checkpt.close();
        }
    }
    return _iterations;
}
 
/**
Simulator::Simulator(const size_t& rows, const size_t& cols):
        _epsilon(.1),
        _matrix(rows, cols) {
    for (size_t i = 1; i < rows - 1; i++){
        for (size_t j = 1; j < cols - 1; j++){
                _matrix.at(i, j) = DEFAULT_CENTER;
        }
    }
}*/

Simulator::Simulator(std::istream& input_stream, const int& checkpoint):
    Simulator(metadata(input_stream)) {
        _checkpoint = checkpoint;
    }

Simulator::Simulator(metadata&& mdata):
        _iterations(mdata.iterations),
        _epsilon(mdata.epsilon),
        _matrix(std::move(mdata.data_array), mdata.num_cols, mdata.num_rows){}

/**
void Simulator::read_from(std::istream* input_stream) {

    auto extract = [&](auto& x) { input_stream->read(reinterpret_cast<char*>(&x), sizeof(x)); };

    metadata::dimension_t row_length = 0, col_length = 0;

    extract(_iterations);
    extract(_epsilon);
    extract(col_length);
    extract(row_length);

    _matrix = Matrix<metadata::data_t, 2>(row_length, col_length);

    for (size_t row = 0; row < col_length; row++) {
        for (size_t col = 0; col < row_length; col++) {
            extract(_matrix.at(row, col));
        }
    }}
*/

void Simulator::write_to(std::ostream& output_stream) const{

    auto insert = [&](auto x) { output_stream.write(reinterpret_cast<char*>(&x), sizeof(x)); };

    metadata::dimension_t row_length = this->row_length(), col_length = this->col_length();

    insert(_iterations);
    insert(_epsilon);
    insert(col_length);
    insert(row_length);

    for (size_t row = 0; row < col_length; row++) {
        for (size_t col = 0; col < row_length; col++) {
            insert(_matrix.at(row, col));
        }
    }
}
