#include <cstdint>
#include <iostream>
#include <memory>

struct metadata {

    typedef uint32_t iteration_t;
    typedef float epsilon_t;
    typedef uint32_t dimension_t;
    typedef float data_t;

    iteration_t iterations;
    epsilon_t epsilon;
    dimension_t num_rows;
    dimension_t num_cols;
    std::unique_ptr<data_t[]> data_array;

    metadata(std::istream& input_stream) {
        auto extract = [&](auto& x) { input_stream.read(reinterpret_cast<char*>(&x), sizeof(x)); };
        extract(iterations);
        extract(epsilon);
        extract(num_rows);
        extract(num_cols);
        data_array.reset(new float[num_rows * num_cols]);
        input_stream.read(reinterpret_cast<char*>(data_array.get()), sizeof(data_t) * num_rows * num_cols);
    }
};
