#ifndef MATRIX_H_
#define MATRIX_H_

#include <memory>
#include <sstream>
#include <assert.h>


#include <algorithm>
#include <iterator>
#include <typeinfo>
#include <array>

template<typename T, size_t DIM> class Matrix {

    typedef unsigned long int index_t;
    typedef unsigned long int dimension_t;

    private:
        std::array<dimension_t, DIM> _dimensions;
        std::unique_ptr<T[]> _matrix;

    public:

        Matrix() {}

        template <typename ... dimension_t> 
        Matrix(std::unique_ptr<T[]>&& matrix, dimension_t ... dimension_sizes):
            _dimensions{(dimension_sizes)...},
            _matrix(std::move(matrix)){
            static_assert(sizeof...(dimension_sizes) == DIM,
                    "Number of dimensions and number of dimension sizes must be the same");
        }

        template <typename ... dimension_t> 
        Matrix(dimension_t ... dimension_sizes):
            _dimensions{(dimension_sizes)...},
            _matrix(new T[size()]()){
            static_assert(sizeof...(dimension_sizes) == DIM,
                    "Number of dimensions and number of dimension sizes must be the same");
        }

        template <typename ... index_t> T& at(const index_t& ... index_pack) {
            static_assert(sizeof...(index_pack) == DIM, "Number of indices and number of dimensions must be the same");
            std::array<dimension_t, DIM> indices = {(index_pack)...};

            size_t position = indices[DIM - 1];
            for (size_t i = 0; i < DIM - 1; i++) {
                position += indices[i] * _dimensions[i];
            }
            return _matrix[position];
        }

        template <typename ... index_t> const T& at(const index_t& ... index_pack) const {
            static_assert(sizeof...(index_pack) == DIM, "Number of indices and number of dimensions must be the same");
            std::array<dimension_t, DIM> indices = {(index_pack)...};

            size_t position = indices[DIM - 1];
            for (size_t i = 0; i < DIM - 1; i++) {
                position += indices[i] * _dimensions[i];
            }
            return _matrix[position];
        }

        size_t size(dimension_t dim) const { return _dimensions[dim]; }

        size_t size() const { return std::accumulate(_dimensions.begin(), _dimensions.end(), 1, std::multiplies<dimension_t>()); }

        class iterator: public std::iterator<std::forward_iterator_tag, T> {

            index_t _index;

            public:

                iterator (size_t index = 0): _index(index) {}

                //Pre increment
                iterator& operator++() { _index++; return *this; }

                //Post increment
                iterator& operator++(int) { iterator tmp(*this); _index++; return tmp; }
        };

        void swap(Matrix<T, DIM>& other) {
            _dimensions.swap(other._dimensions);
            _matrix.swap(other._matrix);
        }
};

#endif
