#include <iostream>
#include "util/metadata.h"
//#include <typeinfo>
#include <limits>
#include <iomanip>
#include <cmath>

//typedef std::conditional<sizeof(float) == sizeof(unsigned int), unsigned int, unsigned long>::type scale_t;
//
typedef unsigned char scale_t;
typedef metadata::data_t data_t;

data_t get_max(const metadata& mdata) {
    data_t current = std::numeric_limits<data_t>::min();
    auto arr = mdata.data_array.get();
    for (size_t i = 0; i < mdata.num_rows * mdata.num_cols; i++)
        current = std::max(current, arr[i]);
    return current;
}

void to_plain_pgm(std::istream& input) {

    metadata mdata(input);

    std::cout << "P2\n" << mdata.num_cols << ' ' << mdata.num_rows << '\n'
                        << (int)std::numeric_limits<scale_t>::max() << '\n';

    data_t max_val = get_max(mdata);
    auto scaled = [&] (size_t i) { return (int)(std::numeric_limits<scale_t>::max() * (mdata.data_array[i] / max_val)); };
    //I was impatient and I decided I would rather print a newline on each
    //Rather than deal with unnecessary calculations
    for (size_t i = 0; i < mdata.num_rows * mdata.num_cols; i++)
        std::cout << scaled(i) << '\n';

}

int main() {

    to_plain_pgm(std::cin);

    return 0;

}
