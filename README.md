Building:

'make all':
Compiles and builds the executables for bin/viewer and bin/simulator

'make optimized all':

Compiles bin/viewer and bin/simulator with optimization turned on.

'make debug all':

Compiles bin/viewer and bin/simulator with debug symbols

Running 'make' with no targets specified will default to running 'make all'.


'make clean' will remove the intermediate object files, the automatic dependencies
thereof, and the executable, should any of these exist.

Running:

Suppose you have an input file named 'state.start', and you want to write it to 'state.end'.
The command would look as 

Executing the simulator works as follows:

simulator _k_ < _state.in_ > _state.out_

where _k_ is how often you want to generate a checkpoint, _state.in_ is an 
existing binary file read from stdin that sets the initial state of the
simulation, and _state.out_ is a file which will contain the final state
redirected from stdout.

if _k_ is unspecified or 0 (default), then no checkpoints will be written.
